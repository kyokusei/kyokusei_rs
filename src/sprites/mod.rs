extern crate gba;

use gba::{
    io::dma,
    Color
};

pub mod tiles;

pub const BLACK: Color = Color::from_rgb(0, 0, 0);
pub const RED: Color = Color::from_rgb(31, 0, 0);
pub const GREEN: Color = Color::from_rgb(0, 31, 0);
pub const BLUE: Color = Color::from_rgb(0, 0, 31);
pub const YELLOW: Color = Color::from_rgb(31, 31, 0);
pub const MAGENTA: Color = Color::from_rgb(31, 0, 31);
pub const CYAN: Color = Color::from_rgb(0, 31, 31);
pub const WHITE: Color = Color::from_rgb(31, 31, 31);

const fn set_color(red: u16, green: u16, blue: u16) -> Color {
    Color::from_rgb(red>>3, green>>3, blue>>3)
}

pub static PALETTE1: [Color; 32] = [
    set_color(0, 0, 0),       // index 0 - transparency {black}
    set_color(34, 32, 52),    // index 1 - near-black
    set_color(69, 40, 60),    // index 2 - dark brown
    set_color(102, 57, 49),   // index 3
    set_color(143, 86, 59),   // index 4
    set_color(223, 113, 38),  // index 5
    set_color(217, 160, 102), // index 6
    set_color(238, 195, 154), // index 7
    set_color(251, 242, 54),  // index 8
    set_color(153, 229, 80),  // index 9
    set_color(106, 190, 48),  // index 10
    set_color(55, 148, 110),  // index 11
    set_color(75, 105, 47),   // index 12
    set_color(82, 75, 36),    // index 13
    set_color(50, 60, 57),    // index 14
    set_color(63, 63, 116),   // index 15
    set_color(48, 96, 130),   // index 16
    set_color(91, 110, 225),  // index 17
    set_color(99, 155, 255),  // index 18
    set_color(95, 205, 228),  // index 19
    set_color(203, 219, 252), // index 20
    set_color(255, 255, 255), // index 21
    set_color(155, 173, 183), // index 22
    set_color(132, 126, 135), // index 23
    set_color(105, 106, 106), // index 24
    set_color(89, 86, 82),    // index 25
    set_color(118, 66, 138),  // index 26
    set_color(172, 50, 50),   // index 27
    set_color(217, 87, 99),   // index 28
    set_color(215, 123, 186), // index 29
    set_color(143, 151, 74),  // index 30
    set_color(138, 11, 48),   // index 31
];

pub static PUMPKIN_SPRITE: [u8; 1024] = *include_bytes!("../../target/assets/sprites/Sprite-0001-Pumpkin.bin");
pub fn update_pumpkin_sprite(index: usize) {
    let offset = index * 0x400;

    unsafe {
        dma::DMA3::set_source((&PUMPKIN_SPRITE as *const u8).offset(index as isize *0x800) as *const u32);
        dma::DMA3::set_dest(0x0601_0000 as *mut u32);
        dma::DMA3::set_count(0x800 / 4);
        dma::DMA3::set_control(
            dma::DMAControlSetting::new()
                .with_use_32bit(true)
                .with_enabled(true)
        );
    }
}
