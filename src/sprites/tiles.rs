pub const TILE_SIZE: usize = 8;
pub const MAX_PLACE_DIMENSION: usize = 32;

type CharData = [u8; 1024]; // the number here is the number of total pixels to deal with in a PNG.

pub struct Tile {
    pub chars: [usize; 4],
    pub solid: bool,
}

pub struct Tileset {
    pub chardata: &'static CharData,
    pub tiles: [Tile; 64],
}

pub struct Place {
    pub tileset: &'static Tileset,
    pub tiles: [[u8; MAX_PLACE_DIMENSION]; MAX_PLACE_DIMENSION],
}