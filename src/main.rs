//! # 極性 -Kyokusei-
//! TODO: add real documentation.

#![no_std]
#![feature(start)]
#![allow(warnings)]

extern crate gba;

use gba::{
    io::{
        display::{DisplayControlSetting, DisplayMode, DISPCNT},
        keypad::read_key_input,
    },
    vram::bitmap::Mode3,
    Color,
};

pub mod util;
pub mod sprites;
pub mod character;
pub mod items;

#[allow(unused_imports)]
use util::{panic, spin_until_vblank, spin_until_vdraw};

#[start]
fn main(_argc: isize, _argv: *const *const u8) -> isize {
    const SETTING: DisplayControlSetting =
        DisplayControlSetting::new().with_mode(DisplayMode::Mode3).with_bg2(true);
    DISPCNT.write(SETTING);

    let mut px = Mode3::WIDTH / 2;
    let mut py = Mode3::HEIGHT / 2;
    let mut color1 = Color::from_rgb(31, 0, 0);
    let mut color2 = Color::from_rgb(0, 31, 0);
    let mut color3 = Color::from_rgb(0, 0, 31);
    let mut color4 = Color::from_rgb(31, 31, 31);

    loop {
        // read our keys for this frame
        let this_frame_keys = read_key_input();

        // adjust game state and wait for vblank
        px = px.wrapping_add(1 * this_frame_keys.x_tribool() as usize);
        py = py.wrapping_add(1 * this_frame_keys.y_tribool() as usize);
        if this_frame_keys.l() {
            color1 = Color(color1.0.rotate_left(1));
            color2 = Color(color2.0.rotate_left(1));
            color3 = Color(color3.0.rotate_left(1));
            color4 = Color(color4.0.rotate_left(1));
        }
        if this_frame_keys.r() {
            color1 = Color(color1.0.rotate_right(1));
            color2 = Color(color2.0.rotate_right(1));
            color3 = Color(color3.0.rotate_right(1));
            color4 = Color(color4.0.rotate_right(1));
        }

        // now we wait
        spin_until_vblank();

        // draw the new game and wait until the next frame starts.
        if px >= Mode3::WIDTH || py >= Mode3::HEIGHT {
            // out of bounds, reset the screen and position.
            Mode3::dma_clear_to(Color::from_rgb(0, 0, 0));
            px = Mode3::WIDTH / 2;
            py = Mode3::HEIGHT / 2;
        } else {
            // draw the new part of the line
            Mode3::write(px, py, color1);
            Mode3::write(px, py + 5, color2);
            Mode3::write(px + 5, py, color3);
            Mode3::write(px + 5, py + 5, color4);
        }

        // now we wait again
        spin_until_vdraw();
    }
}
