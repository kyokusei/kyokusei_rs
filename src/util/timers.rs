extern crate gba;

use gba::{
    vram::bitmap::Mode3,
    io::{
        irq::{self, IrqEnableSetting, IrqFlags, BIOS_IF, IE, IME},
        timers::{TimerControlSetting, TimerTickRate, TM0CNT_H, TM0CNT_L, TM1CNT_H, TM1CNT_L},
    },
    Color,
};
//use super::*;
use super::super::sprites::{BLACK, RED, GREEN, BLUE, YELLOW, MAGENTA};

static mut PIXEL: usize = 0;

pub fn init_timers() {
    let init_val: u16 = u32::wrapping_sub(0x1_0000, 64) as u16;
    const TIMER_SETTINGS: TimerControlSetting =
        TimerControlSetting::new().with_overflow_irq(true).with_enabled(true);

    TM0CNT_L.write(init_val);
    TM0CNT_H.write(TIMER_SETTINGS.with_tick_rate(TimerTickRate::CPU1024));
    TM1CNT_L.write(init_val);
    TM1CNT_H.write(TIMER_SETTINGS.with_tick_rate(TimerTickRate::CPU1024));
}

pub fn write_pixel(color: Color) {
    unsafe {
        Mode3::write(PIXEL, 0, color);
        PIXEL = (PIXEL + 1) % (Mode3::WIDTH * Mode3::HEIGHT);
    }
}

pub extern "C" fn irq_handler(flags: IrqFlags) {
    if flags.vblank() {
        vblank_handler();
    }
    if flags.hblank() {
        hblank_handler();
    }
    if flags.vcounter() {
        vcounter_handler();
    }
    if flags.timer0() {
        timer0_handler();
    }
    if flags.timer1() {
        timer1_handler();
    }
}

pub fn vblank_handler() {
    write_pixel(BLUE);
    BIOS_IF.write(BIOS_IF.read().with_vblank(true));
}

pub fn hblank_handler() {
    write_pixel(GREEN);
    BIOS_IF.write(BIOS_IF.read().with_hblank(true));
}

pub fn vcounter_handler() {
    write_pixel(RED);
    BIOS_IF.write(BIOS_IF.read().with_vcounter(true));
}

pub fn timer0_handler() {
    write_pixel(YELLOW);
    BIOS_IF.write(BIOS_IF.read().with_timer0(true));
}

pub fn timer1_handler() {
    write_pixel(MAGENTA);
    BIOS_IF.write(BIOS_IF.read().with_timer1(true));
}