extern crate gba;

use gba::{
    fatal,
    io::display::{VBLANK_SCANLINE, VCOUNT},
    Color,
};

pub mod geometry;
pub mod timers;

// these colors are here for the time being
pub const BLACK: Color = Color::from_rgb(0, 0, 0);
pub const RED: Color = Color::from_rgb(31, 0, 0);
pub const GREEN: Color = Color::from_rgb(0, 31, 0);
pub const BLUE: Color = Color::from_rgb(0, 0, 31);
pub const YELLOW: Color = Color::from_rgb(31, 31, 0);
pub const MAGENTA: Color = Color::from_rgb(31, 0, 31);
pub const CYAN: Color = Color::from_rgb(0, 31, 31);
pub const WHITE: Color = Color::from_rgb(31, 31, 31);


// TODO: implement and include more modules to assist in developing project. Also, some extra functions.

/// The panic handler for any unrecoverable errors.
/// - `fatal!`is for mGBA development. Will be removed once development is fully complete.
/// - `loop {}` is for errors that occur when *not* in mGBA, i.e. playing on real hardware.
#[panic_handler]
pub fn panic(info: &core::panic::PanicInfo) -> ! {
    fatal!("{}", info);
    loop {}
}

/// Performs a busy loop until VBlank starts.
///
/// This is very inefficient, and will probably be removed once I get the bios calls working
pub fn spin_until_vblank() {
    while VCOUNT.read() < VBLANK_SCANLINE {}
}

/// Performs a busy loop until VDraw starts.
///
/// This is very inefficient, and will probably be removed once I get the bios calls working
pub fn spin_until_vdraw() {
    while VCOUNT.read() >= VBLANK_SCANLINE {}
}