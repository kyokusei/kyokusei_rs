// TODO: create and implement items and item effects.

pub mod healing_items;
pub mod status_items;

enum ItemType {
    Healing,
    StatusHealing,
    StatusInflicter,
    Key,
    Material,
}

struct Item<ItemEffect> where ItemEffect: Fn(u32) -> u32 {
    item_id:        u32,
    item_type:      ItemType,
    item_effect:    ItemEffect,
    item_descript:  &'static str,
    item_sell:      u16,
}

// good to know i can do structs of structs
//struct ItemWrap {
//    item: Item<ItemEffect>,
//}
