use super::{Character, StatusAilment};
//use alloc::string::String;

pub struct Enemy {
    // TODO: implement enemy things
    pub name:      &'static str,
    pub level:     u8,
    pub exp:       usize,
    // pub next_lvl:  usize,
    pub max_hp:    usize,
    pub hp:        isize,
    pub max_mp:    usize,
    pub mp:        isize,
    pub strength:  u8,
    pub dexterity: u8,
    pub magic:     u8,
    pub vitality:  u8,
    pub luck:      u8,
    pub status:    StatusAilment,
    pub is_dead:   bool,
}

impl Character for Enemy {
//    fn new() {}
    fn get_status(&self) -> StatusAilment { self.status }

    fn get_health(&self) -> (isize, usize) { (self.hp, self.max_hp) }

    fn get_mana(&self) -> (isize, usize) { (self.mp, self.max_mp) }

    fn get_stats(&self) -> (u8, u8, u8, u8) {
        (self.strength, self.dexterity,
         self.magic, self.vitality)
    }

    fn take_damage(&mut self, damage: i16) {}
}
