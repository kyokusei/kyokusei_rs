pub mod player;
pub mod enemy;

#[derive(Debug, Copy, Clone)]
pub enum StatusAilment {
    Normal,
    Poison,
    Burning,
    Frost,
    Shock,
    Slow,
    // more here.
}

pub trait Character {
    // TODO: declare common character actor functions

    fn get_status(&self) -> StatusAilment;
    fn get_health(&self) -> (isize, usize);
    fn get_mana(&self) -> (isize, usize);
    fn get_stats(&self) -> (u8, u8, u8, u8);
    fn take_damage(&mut self, damage: i16);
}
