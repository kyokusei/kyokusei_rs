#!/usr/bin/env bash

# ============================================================================================== #



# ██╗  ██╗██╗   ██╗ ██████╗ ██╗  ██╗██╗   ██╗███████╗███████╗██╗
# ██║ ██╔╝╚██╗ ██╔╝██╔═══██╗██║ ██╔╝██║   ██║██╔════╝██╔════╝██║
# █████╔╝  ╚████╔╝ ██║   ██║█████╔╝ ██║   ██║███████╗█████╗  ██║
# ██╔═██╗   ╚██╔╝  ██║   ██║██╔═██╗ ██║   ██║╚════██║██╔══╝  ██║
# ██║  ██╗   ██║   ╚██████╔╝██║  ██╗╚██████╔╝███████║███████╗██║
# ╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚══════╝╚═╝


# ---------------------------------------------------------------------------------------------- #
# author: aurumcodex                                                                             #
# version: 0.0.0                                                                                 #
#                                                                                                #
# (Script Last Updated on: 2019.5.23)                                                            #
# (Project Last Updated on: 2019.5.19)                                                           #
#                                                                                                #
# [Build Script for compiling the game.]                                                         #
# ============================================================================================== #

# //! TODO: add a help function, perhaps? Eh, it'll be on the backburner for a bit.

## begin global declarations.
# //! TODO: clean unused variables. (and determine which *are* used.)

# This looks garbled here, but displays correctly.
readonly Title="
_  _ _   _ ____ _  _ _  _ ____ ____ _
|_/   \\_/  |  | |_/  |  | [__  |___ |
| \\_   |   |__| | \\_ |__| ___] |___ |
_____________________________________
"
# yes, there are spaces here.
readonly VersionInfo="      version: 0.0.0-pre.alpha\n"
# Color escape characters for prettying up script output.
readonly testchar='◊'
readonly ClrReset='\033[0m';  readonly ClrBold='\033[1m';       readonly ClrInvert='\033[7;1m';
readonly ClrRed='\033[0;31m'; readonly ClrBoldRed='\033[1;31m'; readonly ClrHIRed='\033[0;91m';
readonly ClrGrn='\033[0;32m'; #readonly ClrBoldGrn='\033[1;32m';
readonly ClrYlw='\033[0;33m'; readonly ClrBoldYlw='\033[1;33m';
readonly ClrBlu='\033[0;34m'; readonly ClrBoldBlu='\033[1;34m'; readonly ClrInvBlue='\033[7;1;34m';
readonly ClrMgt='\033[0;35m'; readonly ClrBoldMgt='\033[1;35m';
readonly ClrCyn='\033[0;36m'; readonly ClrBoldCyn='\033[1;36m';

# Some file locations and the like.
readonly CrtFile=components/crt0.s
readonly CrtOutput=target/crt0.o

readonly SpriteDir=target/assets/sprites
readonly TilemapDir=target/assets/tilemaps

readonly TargetJSON=components/thumbv4-none-agb.json
readonly TargetDir=target/thumbv4-none-agb
readonly TargetGBA=target/kyokusei.gba

# Default Argument(s)
readonly ARGS=${1-debug}
## end global declarations.

__print_help() {
  printf "TODO: implement some helpful stuff.\n"
}

__check_status() {
  if [[ $? == 0 ]]; then
    printf "${ClrGrn}Done.${ClrReset}\n"
  elif [[ $? == 127 ]]; then
    printf "${ClrBoldRed}Error${ClrReset}: ${ClrHIRed}%s${ClrReset}\n%s\n" \
        "required tool not found." "Please add /opt/devkitpro/devkitARM/bin to PATH variable."
    exit 127
  elif [[ $? > 0 && $? != 127 ]]; then
    printf "$\n{ClrBoldRed}Error${ClrReset}: ${ClrHIRed}%s${ClrReset}\n" \
        "Some error has occured. Stopping script."
    exit $?
  fi
}

__compile_crt0() {
  if [[ ! -f ${CrtOutput} ]]; then
    if [[ ! -d target/ ]]; then
      printf "Creating target directory ... "
      mkdir target/
      __check_status
    fi
    printf "${ClrBoldBlu}Compiling${ClrReset}: ${ClrBold}crt0.s bootloader${ClrReset} ... "
    arm-none-eabi-as ${CrtFile} -o ${CrtOutput}
    __check_status
  else
    printf "${ClrBoldYlw}NOTE${ClrReset}: %s\n" \
          "Object crt0.o exists. Using existing file."
  fi
}

__make_assets() {
  if [[ ! -d target/assets/sprites || ! -d target/assets/tilemaps ]]; then
    printf "${ClrYlw}Target asset directories not found.${ClrReset} Creating ... "
    mkdir -p target/assets/sprites ; mkdir -p target/assets/tilemaps
    __check_status
  fi

  __build_sprites ; __build_tilemaps
}

__build_sprites() {
  # //! TODO: determine the various types of sprites that are going to be used. This is a start.
  # //! Maybe try and abstract this out? Dunno if it'd be worth it, though.
  local aseprite_pttr="\.aseprite$"
  local sprite_pttr="^Sprite-.*\.png$"
  local plyr_spr_pttr="^plyr_.*\.png$"
  local item_spr_pttr="^item_.*\.png$"
  local npc_spr_pttr="^npc_.*\.png$"
  local boss_spr_pttr="^boss_.*\.png$"
  local enemy_spr_pttr="^emny_.*\.png$"
  local ui_spr_pttr="^ui_.*\.png$"

  for sprite in components/assets/sprites/*; do
    printf "${ClrInvert}DEBUGGING :: ${ClrReset} $(basename ${sprite})\n"
    # if ${sprite} is a "neutral" sprite
    if [[ "$(basename ${sprite})" =~ ${aseprite_pttr} ]]; then
      printf "Ignoring for *.aseprite files for now.\n"
    elif [[ "$(basename ${sprite})" =~ ${sprite_pttr} ]]; then
      printf "${ClrBoldBlu}Converting unknown sprite${ClrReset}: ${sprite} ... "
      ./components/scripts/png_tile_converter.py \
        ${sprite} 32 32 -o ${SpriteDir}/$(basename ${sprite%.*}).bin
      __check_status
    # if ${sprite} is a "Player" sprite
    elif [[ "$(basename ${sprite})" =~ ${plyr_spr_pttr} ]]; then
      printf "${ClrBoldBlu}Converting Player sprite${ClrReset}: ${sprite} ... "
      ./components/scripts/png_tile_converter.py \
        ${sprite} 32 32 -o ${SpriteDir}/$(basename ${sprite%.*}).bin
      __check_status
    # if ${sprite} is an "Item" sprite
    elif [[ "$(basename ${sprite})" =~ ${item_spr_pttr} ]]; then
      printf "${ClrBoldBlu}Converting Item sprite${ClrReset}: ${sprite} ... "
      ./components/scripts/png_tile_converter.py \
        ${sprite} 32 32 -o ${SpriteDir}/$(basename ${sprite%.*}).bin
     __check_status
    # if ${sprite} is an "NPC" sprite
    elif [[ "$(basename ${sprite})" =~ ${npc_spr_pttr} ]]; then
      printf "${ClrBoldBlu}Converting NPC sprite${ClrReset}: ${sprite} ... "
      ./components/scripts/png_tile_converter.py \
        ${sprite} 32 32 -o ${SpriteDir}/$(basename ${sprite%.*}).bin
     __check_status
    # if ${sprite} is a "Boss" sprite
    elif [[ "$(basename ${sprite})" =~ ${boss_spr_pttr} ]]; then
      printf "${ClrBoldBlu}Converting Boss sprite${ClrReset}: ${sprite} ... "
      ./components/scripts/png_tile_converter.py \
        ${sprite} 32 32 -o ${SpriteDir}/$(basename ${sprite%.*}).bin
     __check_status
    # if ${sprite} is an "Enemy" sprite
    elif [[ "$(basename ${sprite})" =~ ${enemy_spr_pttr} ]]; then
      printf "${ClrBoldBlu}Converting Enemy sprite${ClrReset}: ${sprite} ... "
      ./components/scripts/png_tile_converter.py \
        ${sprite} 32 32 -o ${SpriteDir}/$(basename ${sprite%.*}).bin
     __check_status
    # if ${sprite} is a "UI" sprite
    elif [[ "$(basename ${sprite})" =~ ${ui_spr_pttr} ]]; then
      printf "${ClrBoldBlu}Converting UI sprite${ClrReset}: ${sprite} ... "
      ./components/scripts/png_tile_converter.py \
        ${sprite} 32 32 -o ${SpriteDir}/$(basename ${sprite%.*}).bin
     __check_status
    # Some kind of sprite error.
    else
      printf "${ClrBoldRed}Error getting sprite(s).${ClrReset}\n"
      exit 1
    fi
  done
}

__build_tilemaps() {
  # //! TODO: effectively the same as the __build_sprites function.
  local tile_pttr="^tilemap_.*\.png$"
  local bg_tile_pttr="^bg_tile_.*\.png$"

  for tilemap in components/assets/tilemaps/*; do
    # if ${tilemap} is an actual tilemap
    if [[ "${tilemap}" =~ ${tile_pttr} ]]; then
      printf "${ClrBoldBlu}s${ClrReset}: ${ClrBold}${tilemap}${ClrReset} ... " \
          "Converting tilemap"
      ./components/scripts/png_tile_converter.py \
        ${tilemap} 8 8 -o ${TilemapDir}/$(basename ${sprite%.*}).bin
      __check_status
    # if ${tilemap} is instead a background sprite
    elif [[ "${tilemap}" =~ ${bg_tile_pttr} ]]; then
      printf "${ClrBoldBlu}%s${ClrReset}: ${ClrBold}${tilemap}${ClrReset}" \
          "Converting background sprite"
    fi
  done
}

__convert_to_gba() {
  printf "\n${ClrBoldMgt}Copying objects ... ${ClrReset}"
  if [[ ${ARGS} == debug ]]; then
    arm-none-eabi-objcopy -O binary ${TargetDir}/debug/kyokusei ${TargetGBA}
    __check_status
  elif [[ ${ARGS} == release ]]; then
    arm-none-eabi-objcopy -O binary ${TargetDir}/release/kyokusei ${TargetGBA}
    __check_status
  fi

  printf "${ClrBoldMgt}Fixing ROM header ... ${ClrReset}"
  gbafix target/kyokusei.gba
  # if [[ $? -ge 1 ]];
}

__compile() {
  local local_args=${1-debug}
  __compile_crt0
  __make_assets
  case ${local_args} in
    "debug")
      printf "\n${ClrYlw}Initializing Development Build ...${ClrReset}\n\n"
      cargo xbuild --target ${TargetJSON}
      __check_status ;;
    "release")
      printf "\n${ClrYlw}Initializing Release Build ...${ClrReset}\n\n"
      cargo xbuild --target ${TargetJSON} --release
      __check_status ;;
    *)
      printf "\n
      ${ClrBoldRed}Error${ClrReset}: ${ClrRed}Invalid argument found.${ClrReset}\n"
      exit 1 ;;
  esac
  __convert_to_gba
}

__main() {
  # This section parses the arguments.
  if [[ $# > 1 ]]; then
    printf "${ClrBoldRed}Error${ClrReset}: ${ClrHIRed}Argument count over 1.${ClrReset}\n" >&2
    exit 1
  else
    case ${ARGS} in
      "clean"|"c")
        printf "${ClrBoldBlu}Cleaning${ClrReset}: ${ClrBold}target directory${ClrReset} ... "
        rm -rf target/*
        __check_status ;;
      "debug"|"d")
        printf "${ClrBoldCyn}${Title}${ClrReset}${ClrCyn}${VersionInfo}${ClrReset}\n"
        printf "\n${ClrMgt}Compiling${ClrReset}: Development Build\n\n"
        __compile
        if [[ $? == 0 ]]; then
          printf "\n${ClrInvBlue}◊ Build Finished.${ClrReset} Time: ${SECONDS} seconds.\n\n"
        fi ;;
      "release"|"r")
        printf "${ClrBoldCyn}${Title}${ClrCyn}${VersionInfo}${ClrReset}"
        printf "\n${ClrMgt}Compiling${ClrReset}: Release Build\n\n"
        __compile release
        if [[ $? == 0 ]]; then
          printf "\n${ClrInvBlue}◊ Build Finished.${ClrReset} Time: ${SECONDS} seconds.\n\n"
        fi ;;
      "help"|"h")
        __print_help ;;
      *)
        printf "\n${ClrBoldRed}Error${ClrReset}: ${ClrHIRed}Invalid argument.${ClrReset}\n" >&2
        exit 1 ;;
    esac
  fi
}

__main $@
